sap.ui.define([
    "sap/ui/core/mvc/Controller"
],
    /**
     * @param {typeof sap.ui.core.mvc.Controller} Controller
     */
    function (Controller) {
        "use strict";

        return Controller.extend("FioriApplication.freestyleapps.cky.training.controller.Main", {
            onInit: function () {

            }
        });
    });
