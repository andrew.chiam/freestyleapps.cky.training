/* global QUnit */
QUnit.config.autostart = false;

sap.ui.getCore().attachInit(function () {
	"use strict";

	sap.ui.require([
		"FioriApplication/freestyleapps.cky.training/test/unit/AllTests"
	], function () {
		QUnit.start();
	});
});
