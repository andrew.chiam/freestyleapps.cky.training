/* global QUnit */

sap.ui.require(["FioriApplication/freestyleapps/cky/training/test/integration/AllJourneys"
], function () {
	QUnit.config.autostart = false;
	QUnit.start();
});
